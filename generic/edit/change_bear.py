from requests import put
from json import dumps
from allure import step

from generic.endpoints import endpoint_bear
from generic.read import read_bears
from settings import ITEM


@step("Changing the values of an existing bear")
def change_bear(bear_id, new_bear_data):
    put("{}/{}".format(endpoint_bear(), bear_id), data=dumps(new_bear_data))
    return read_bears().json()[ITEM]
