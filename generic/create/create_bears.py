from requests import post
from allure import step
from json import dumps

from generic.endpoints import endpoint_bear
from generic.read import read_bears
from settings import ITEM


@step("Create data for a bear")
def bear_data(bear_type="BLACK", bear_name="MIKHAIL", bear_age=17.5):
    return {
        "bear_type": bear_type,
        "bear_name": bear_name,
        "bear_age": bear_age,
    }


@step("Create a new bear with specified parameters")
def new_bear(new_bear_data):
    post(endpoint_bear(), data=dumps(new_bear_data))
    return read_bears().json()[ITEM]


@step("Create a list of many bears")
def create_multiple_bears(number_of_bears):
    for _ in range(number_of_bears):
        post(endpoint_bear(), data=dumps(bear_data()))
