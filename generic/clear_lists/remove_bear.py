from requests import delete
from allure import step

from generic.endpoints import endpoint_bear


@step("Remove any existing bears")
def remove_all_bears():
    return delete(endpoint_bear())
