from requests import get
from allure import step

from generic.endpoints import endpoint_bear


@step("Read the list of all bears")
def read_bears():
    return get(endpoint_bear())
