from requests import get
from allure import step

from generic.endpoints import endpoint_info


@step("Read the service description")
def get_info():
    return get(endpoint_info())
