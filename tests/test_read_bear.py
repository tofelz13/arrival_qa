from hamcrest import assert_that, equal_to

from generic.read import read_bears
from generic.create import new_bear, bear_data
from settings import ITEM, BEAR_ID, STATUS_OK


def test_read_bear_response_code():
    assert_that(read_bears().status_code, equal_to(STATUS_OK))


def test_read_empty_bear_list():
    assert_that(read_bears().json(), equal_to([]))


def test_read_bear():
    bear_id = new_bear(bear_data())[BEAR_ID]
    assert_that(read_bears().json()[ITEM][BEAR_ID], equal_to(bear_id))
