from pytest import fixture

from generic.clear_lists import remove_all_bears


@fixture
def cleaning():
    yield
    remove_all_bears()
