from pytest import fixture
from hamcrest import assert_that, equal_to

from generic.create import new_bear, bear_data
from settings import BEAR_TYPE, BEAR_NAME, BEAR_AGE


@fixture(
    params=[
        (bear_data(bear_type="BLACK"), "BLACK"),
        (bear_data(bear_type="POLAR"), "POLAR"),
        (bear_data(bear_type="BROWN"), "BROWN"),
        (bear_data(bear_type="GUMMY"), "GUMMY"),
    ]
)
def create_bear_type(request):
    yield request.param


@fixture(
    params=[
        (bear_data(), "MIKHAIL"),
        (bear_data(bear_name="МИША"), "МИША"),
        (bear_data(bear_name=123), "123"),
    ]
)
def create_bear_name(request):
    yield request.param


@fixture(
    params=[
        (bear_data(), 17.5),
        (bear_data(bear_age=0), 0),
        (bear_data(bear_age=100), 100),
        (bear_data(bear_age=1 / 365), 1 / 365),
    ]
)
def create_bear_age(request):
    yield request.param


def test_bear_type(cleaning, create_bear_type):
    new_bear_data, expected_result = create_bear_type
    assert_that(new_bear(new_bear_data)[BEAR_TYPE], equal_to(expected_result))


def test_bear_name(cleaning, create_bear_name):
    new_bear_data, expected_result = create_bear_name
    assert_that(new_bear(new_bear_data)[BEAR_NAME], equal_to(expected_result))


def test_bear_age(cleaning, create_bear_age):
    new_bear_data, expected_result = create_bear_age
    assert_that(new_bear(new_bear_data)[BEAR_AGE], equal_to(expected_result))
