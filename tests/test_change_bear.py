from hamcrest import assert_that, equal_to
from pytest import fixture

from settings import BEAR_TYPE, BEAR_AGE, BEAR_NAME, BEAR_ID
from generic.create import bear_data
from generic.edit import change_bear
from generic.create import new_bear


@fixture(params=["POLAR", "BROWN", "GUMMY"])
def new_type(request):
    return request.param


@fixture(params=["-Ma$ha-", "Вини пух", "007"])
def new_name(request):
    return request.param


@fixture(params=[0, 23.4, 100])
def new_age(request):
    return request.param


def test_change_bear_type(cleaning, new_type):
    bear_id = new_bear(bear_data())[BEAR_ID]
    assert_that(
        change_bear(bear_id, bear_data(bear_type=new_type))[BEAR_TYPE],
        equal_to(new_type),
    )


def test_change_bear_name(cleaning, new_name):
    bear_id = new_bear(bear_data())[BEAR_ID]
    assert_that(
        change_bear(bear_id, bear_data(bear_name=new_name))[BEAR_NAME],
        equal_to(new_name),
    )


def test_change_bear_age(cleaning, new_age):
    bear_id = new_bear(bear_data())[BEAR_ID]
    assert_that(
        change_bear(bear_id, bear_data(bear_age=new_age))[BEAR_AGE],
        equal_to(new_age),
    )
