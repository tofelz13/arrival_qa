from hamcrest import assert_that, equal_to
from pytest import fixture

from settings import STATUS_OK
from generic.read import read_bears
from generic.create import create_multiple_bears
from generic.clear_lists import remove_all_bears


@fixture(params=[1, 12, 50])
def set_bear_number(request):
    return request.param


def test_remove_response_code():
    assert_that(remove_all_bears().status_code, equal_to(STATUS_OK))


def test_delete_all_bear(set_bear_number):
    create_multiple_bears(set_bear_number)
    remove_all_bears()
    assert_that(read_bears().json(), equal_to([]))
