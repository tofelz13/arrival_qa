from requests import get
from hamcrest import assert_that, equal_to

from generic.read import get_info
from settings import INFO_TEXT, STATUS_OK


def test_info_response_code():
    assert_that(get_info().status_code, equal_to(STATUS_OK))


def test_info_text():
    assert_that(get_info().text, equal_to(INFO_TEXT))


def test_info_type():
    assert_that(type(get_info().text), equal_to(str))
