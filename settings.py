from os import environ
from pyhocon import ConfigFactory

conf = ConfigFactory.parse_file("reference.conf")
HOST = environ.get("TEST_STAND_HOST", conf.get_string("host"))
PORT = environ.get("TEST_STAND_PORT", conf.get_string("port"))
URL = u"{}:{}".format(HOST, PORT)

STATUS_OK = 200
INFO = "/info"
BEAR = "/bear"
ITEM = 0
BEAR_ID = "bear_id"
BEAR_TYPE = "bear_type"
BEAR_NAME = "bear_name"
BEAR_AGE = "bear_age"
INFO_TEXT = """Welcome to Alaska!
This is CRUD service for bears in alaska.
CRUD routes presented with REST naming notation:

POST\t\t\t/bear - create
GET\t\t\t/bear - read all bears
GET\t\t\t/bear/:id - read specific bear
PUT\t\t\t/bear/:id - update specific bear
DELETE\t\t\t/bear - delete all bears
DELETE\t\t\t/bear/:id - delete specific bear

Example of ber json: {"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}.
Available types for bears are: POLAR, BROWN, BLACK and GUMMY."""
