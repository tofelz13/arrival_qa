## 1. Description ```GET /info```
### check that on this endpoint we get information about what the service under test is 
    1. Response code (expected: '200 OK')
    2. Response text
        2.1 A description of what this service is
        2.2 List of CRUD routes
        2.3 Example of bear json ({"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5})
        2.4 List of available bear types (POLAR, BROWN, BLACK and GUMMY)
        2.5 The information in the response is formatted correctly
        2.6 There are no grammatical and punctuation errors)
    3. Wrong method for this endpoint (POST, PUT, DELETE, HEAD)
    4. The information in the response is of the data type 'text/html'
    5. The information in the response is charset=utf-8
    6. HTTP version 1.1
    7. After a GET request the data in the system are not changed 

## 2. Create bears ```POST /bear```
### check that we can create a list of new bears and they are displayed correctly 
    1. Response code (expected: '200 OK')
    2. Create a correct bear from example
    3. After creating the bear in the response, we get its id
    4. Create bears of all available types (POLAR, BROWN, BLACK and GUMMY)
    5. Create bears not available types (PANDA, TREMARCTOS ORNATUS, ...)
    6. Create bears with different names
    7. Create bears with the same name
    8. Create bears of different ages (17.5, 1, 0, 100, 1/365, -1, 101, 1 234 567 890, - 65 538, ...)
    9. Create a bear with missing values
    10. Create a bear with empty values
    11. Create a bear with special character values (*&^%$#@!, ...)
    12. Create a bear with values with spaces
    13. Create many bears (large number of entities)
    14. Create, delete and re-create bear/bears
    15. Try to create a bear with the wrong endpoint
    16. Try to create a bear with double keys
    17. Payload with invalid model
    18. Payload with incomplete model (missing fields or required nested entities)
    19. Check that all created bears are in the list
    20. Make sure that only the bears we created are on the list

## 3. Read bears ```GET /bear```
### check that we can get a list with the bears created, in the correct format
    1. Reading an empty bear list (expected: [])
    2. Reading a list with one bear
    3. Reading a list with many bears, check how many bears are on the list
    4. Reading a specific bear (GET /bear/:id)
    5. All of the bears on the list have "bear_type"
    6. All of the bears on the list have "bear_name"
    7. All of the bears on the list have "bear_age"
    8. All of the bears on the list have "bear_id"
    9. Data type
    10. Reading a specific bear with a wrong id
    11. Response code (expected: '200 OK')
    12. The information in the response is charset=utf-8
    13. HTTP version 1.1
    14. After a GET request the data in the system are not changed 
    

## 4. Update a bear ```PUT /bear/:id```
### check that we can change the values for the previously created bears
    1. Response code (expected: '200 OK')
    2. Change the existing bear
    3. Change "bear_type" to valid values (POLAR, BROWN, BLACK and GUMMY)
    4. Change "bear_name" to valid values 
    5. Change "bear_age" to valid values 
    6. Change "bear_type": to an unapproved value (PANDA, TREMARCTOS ORNATUS, ...)
    7. Try to change an id that does not exist
    8. Try to change the deleted id
    9. Try changing the bear by using double keys (ex. {"bear_type":"BLACK", "bear_type":"BROWN","bear_name":"mikhail","bear_age":17.5})
    10. Try changing the bear without data
    11. Try changing the bear with the incorrect json
    12. Wrong method for this endpoint (POST, HEAD)
    13. Change "bear_name" to invalid values ("", [], {}, null, /n, long string, ...)
    14. Change "bear_age" to invalid values (-66000, -1, 1.0E100500, null, ...)
    15. Payload with incomplete model (missing fields or required nested entities)
    16. The bear changes affect only the expected bear
    17. Changes do not create a new record, but change an existing one

## 5. Delete bears ```DELETE /bear```
### check that we can delete a specific bear, as well as clear all the list
    1. Response code (expected: '200 OK')
    2. In the response we get 'OK'
    3. Delete an existing bear 
    4. After deleting a specific bear, the other bears on the list remain
    5. Delete all bears
    6. Try to delete using the wrong id
    7. Try to delete a deleted bear
    8. Try to delete a wrong endpoint (/info)
    